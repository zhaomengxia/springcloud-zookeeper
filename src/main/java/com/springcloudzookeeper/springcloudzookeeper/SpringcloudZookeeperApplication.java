package com.springcloudzookeeper.springcloudzookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudZookeeperApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudZookeeperApplication.class, args);
	}

}
