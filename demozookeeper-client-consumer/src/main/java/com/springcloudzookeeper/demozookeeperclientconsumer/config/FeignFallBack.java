package com.springcloudzookeeper.demozookeeperclientconsumer.config;

import com.springcloudzookeeper.demozookeeperclientconsumer.service.UserService;
import org.springframework.stereotype.Component;

/**
 * @Author zhaomengxia
 * @create 2019/9/5 11:50
 */
@Component
public class FeignFallBack implements UserService{
    @Override
    public String getUserInfo(Long id) {
        return null;
    }
}
