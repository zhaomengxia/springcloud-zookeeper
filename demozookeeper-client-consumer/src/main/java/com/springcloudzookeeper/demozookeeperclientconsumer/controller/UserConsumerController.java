package com.springcloudzookeeper.demozookeeperclientconsumer.controller;

import com.springcloudzookeeper.demozookeeperclientconsumer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhaomengxia
 * @create 2019/9/5 11:51
 */
@RestController
public class UserConsumerController {
    @Autowired
    private UserService userService;

    @GetMapping("getUser/{id}")
    public String getUserInfo(@PathVariable Long id){
        return userService.getUserInfo(id);
    }
}
