package com.springcloudzookeeper.demozookeeperclientconsumer.service;

import com.springcloudzookeeper.demozookeeperclientconsumer.config.FeignFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author zhaomengxia
 * @create 2019/9/5 11:34
 */
@FeignClient(value = "zookeeper-user-provider",fallback = FeignFallBack.class)
public interface UserService {

    @GetMapping("user")
    String getUserInfo(@RequestParam(value = "id") Long id);
}
