package com.springcloudzookeeper.zookeeperclientprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ZookeeperClientProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZookeeperClientProviderApplication.class, args);
	}

}
