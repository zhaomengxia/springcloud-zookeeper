package com.springcloudzookeeper.zookeeperclientprovider.controller;

import com.springcloudzookeeper.zookeeperclientprovider.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhaomengxia
 * @create 2019/9/5 11:22
 */
@RestController
public class UserController {

    @Value("${server.port}")
    private int port;

    @GetMapping("user")
    public String getUserInfo(@RequestParam("id") Long id){

        User user=new User();
        user.setId(id);
        user.setName("userName"+id);
        return user.toString()+"port:"+port;
    }
}
